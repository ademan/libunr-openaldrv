/*===========================================================================*\
|*  libunr-OpenALDrv - An OpenAL Audio Device for libunr                     *|
|*  Copyright (C) 2018-2019  Adam W.E. Smith                                 *|
|*                                                                           *|
|*  This program is free software: you can redistribute it and/or modify     *|
|*  it under the terms of the GNU General Public License as published by     *|
|*  the Free Software Foundation, either version 3 of the License, or        *|
|*  (at your option) any later version.                                      *|
|*                                                                           *|
|*  This program is distributed in the hope that it will be useful,          *|
|*  but WITHOUT ANY WARRANTY; without even the implied warranty of           *|
|*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            *|
|*  GNU General Public License for more details.                             *|
|*                                                                           *|
|*  You should have received a copy of the GNU General Public License        *|
|*  along with this program. If not, see <https://www.gnu.org/licenses/>.    *|
\*===========================================================================*/

/*========================================================================
* OpenALDrv.h - Class declarations and functions
* 
* written by Adam 'Xaleros' Smith
*========================================================================
*/

#include <Engine/UAudio.h>
#include "AL/al.h"
#include "AL/alc.h"

#define AL_FORMAT_UNKNOWN 0xFFFF

class DLL_EXPORT UOpenALAudioDevice : public UAudioSubsystem
{
  DECLARE_NATIVE_CLASS( UOpenALAudioDevice, UAudioSubsystem, CLASS_NoExport, OpenALDrv )

  UOpenALAudioDevice();

  virtual bool Init();
  virtual bool Exit();
  virtual void Tick( float DeltaTime );
  virtual bool SetOutputDevice( const char* Name );
  virtual void SetViewport( UViewport* Viewport );
  virtual void RegisterSound( USound* Sound );
  virtual void RegisterMusic( UMusic* Music );
  virtual void UnregisterSound( USound* Sound );
  virtual void UnregisterMusic( UMusic* Music );
  virtual bool PlaySound( AActor* Actor, USound* Sound, FVector Location, float Volume, float Radius, float Pitch );

  ALCdevice*  ALDevice;
  ALCcontext* ALContext;
  TArray<ALuint> Sources;
  ALuint MusicSource;
  ALuint* ALMusicBuffers;
  int MaxSources;
  ALenum Error;

protected:
  virtual bool StartMusicPlayback();
  virtual bool PlayMusicBuffer();

  bool ALIsError();
  const char* GetALErrorString();
  ALenum GetALMusicFormat();
};
