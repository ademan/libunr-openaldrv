/*===========================================================================*\
|*  libunr-OpenALDrv - An OpenAL Audio Device for libunr                     *|
|*  Copyright (C) 2018-2019  Adam W.E. Smith                                 *|
|*                                                                           *|
|*  This program is free software: you can redistribute it and/or modify     *|
|*  it under the terms of the GNU General Public License as published by     *|
|*  the Free Software Foundation, either version 3 of the License, or        *|
|*  (at your option) any later version.                                      *|
|*                                                                           *|
|*  This program is distributed in the hope that it will be useful,          *|
|*  but WITHOUT ANY WARRANTY; without even the implied warranty of           *|
|*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            *|
|*  GNU General Public License for more details.                             *|
|*                                                                           *|
|*  You should have received a copy of the GNU General Public License        *|
|*  along with this program. If not, see <https://www.gnu.org/licenses/>.    *|
\*===========================================================================*/

/*========================================================================
* OpenALDrv.cpp - Audio device functionality
* 
* written by Adam 'Xaleros' Smith
*========================================================================
*/

#include <Util/FConfig.h>
#include "OpenALDrv.h"

// TODO: Make use of alcGetError()
UOpenALAudioDevice::UOpenALAudioDevice()
  : UAudioSubsystem()
{
}

UOpenALAudioDevice::~UOpenALAudioDevice()
{
}

bool UOpenALAudioDevice::Init()
{
  if ( bInitialized )
    return true;

  ALContext = NULL;

  // TODO: Add device enumeration and specification configurable via libunr.ini
  ALDevice = alcOpenDevice( NULL );
  if ( ALDevice == NULL )
  {
    GLogf( LOG_ERR, "Failed to initialize OpenALAudioDevice: alcOpenDevice returned NULL" );
    return false;
  }

  // Get config variables (and set defaults if needed)
  SoundVolume = GLibunrConfig->ReadUInt8( "OpenALDrv.OpenALAudioDevice", "SoundVolume", 0, 96 );
  MusicVolume = GLibunrConfig->ReadUInt8( "OpenALDrv.OpenALAudioDevice", "MusicVolume", 0, 96 );
  OutputRate  = GLibunrConfig->ReadInt32( "OpenALDrv.OpenALAudioDevice", "OutputRate", 0, 44100 );

  MusicBufferSize = GLibunrConfig->ReadUInt32( "OpenALDrv.OpenALAudioDevice", "MusicBufferSize", 0, 256 );
  MusicBufferCount = GLibunrConfig->ReadUInt32( "OpenALDrv.OpenALAudioDevice", "MusicBufferCount", 0, 16 );

  // TODO: Cap MusicBufferSize and MusicBufferCount at some hardcoded values.
  // We don't need config files getting corrupted, and then people wondering what happened.

  GLibunrConfig->Save();

  // Initialize parent audio state after reading config variables
  if ( !Super::Init() )
  {
    GLogf( LOG_CRIT, "UAudioSubsystem::Init() failed!" );
    return false;
  }

  // TODO: Make these options configurable as well
  const ALCint ContextAttributes[] =
  {
    ALC_FREQUENCY, OutputRate,
    ALC_MONO_SOURCES, 0,
    ALC_STEREO_SOURCES, 1,
    ALC_REFRESH, 30,
    ALC_SYNC, ALC_FALSE
  };

  ALContext = alcCreateContext( ALDevice, ContextAttributes );
  if ( ALContext == NULL )
  {
    GLogf( LOG_ERR, "Failed to initialize OpenALAudioDevice: alcCreateContext returned NULL" );
    return false;
  }

  if ( alcMakeContextCurrent( ALContext ) == ALC_FALSE )
  {
    GLogf( LOG_ERR, "Failed to initialize OpenALAudioDevice: alcMakeContextCurrent failed" );
    return false;
  }

  // TODO: Dynamic listener based on camera position
  // Set up temporary listener
  ALfloat listenerOri[] = { 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f };
  alListener3f( AL_POSITION, 0, 0, 0.0f );
  alListener3f( AL_VELOCITY, 0, 0, 0 );
  alListenerfv( AL_ORIENTATION, listenerOri );

  // Generate music buffers
  alGenSources( 1, &MusicSource );

  ALMusicBuffers = new ALuint[MusicBufferCount];
  alGenBuffers( MusicBufferCount, ALMusicBuffers );

  bInitialized = true;
  return true;
}

bool UOpenALAudioDevice::Exit()
{
  // TODO: Unregister all sounds and music

  alcDestroyContext( ALContext );
  if ( alcCloseDevice( ALDevice ) == ALC_FALSE )
    GLogf( LOG_ERR, "OpenAL: alcCloseDevice returned ALC_INVALID_DEVICE?" );

  return true;
}

void UOpenALAudioDevice::Tick( float DeltaTime )
{
  Super::Tick( DeltaTime );

  // Update listener position
  // TODO

  // Check current sound 
  /*static TArray<ALuint>::iterator Source = Sources.begin();

  ALint Status;
  int NumChecks = (Sources.Size() > MaxSources) ? 4 : 1;
  while ( NumChecks )
  {
    if ( Source == Sources.end() )
    {
      Source = Sources.begin();
      continue;
    }

    alGetSourcei( *Source, AL_SOURCE_STATE, &Status );
    if ( Status != AL_PLAYING )
    {
      alDeleteSources( 1, &*Source );
      Sources.erase( Source );
    }

    Source++;
  }*/
}

bool UOpenALAudioDevice::SetOutputDevice( const char* Name )
{
  // TODO
  return false;
}

void UOpenALAudioDevice::SetViewport( UViewport* Viewport )
{
  // TODO
}

void UOpenALAudioDevice::RegisterSound( USound* Sound )
{
  int Err = 0;

  if ( !Sound->PcmData )
  {
    Sound->GetRawPcm();
    if ( Sound->PcmData == NULL )
    {
      GLogf( LOG_WARN, "UOpenALAudioDevice::RegisterSound() failed: Sound '%s' has unknown format", Sound->Name.Data() );
      return;
    }
  }

  ALenum Format;
  switch ( Sound->BitsPerSample )
  {
    case 16:
      Format = ( Sound->NumChannels > 1 ) ? AL_FORMAT_STEREO16 : AL_FORMAT_MONO16;
      break;
    case 8:
      Format = ( Sound->NumChannels > 1 ) ? AL_FORMAT_STEREO8 : AL_FORMAT_MONO8;
      break;
    default:
      GLogf( LOG_WARN, "UOpenALAudioDevice::RegisterSound() failed: Sound '%s' has unknown BitsPerSample value", Sound->Name.Data() );
      return;
  }

  alGenBuffers( 1, &Sound->Handle );
  if ( (Err = alGetError()) < 0 ) 
  {
    GLogf( LOG_WARN, "UOpenALAudioDevice::RegisterSound() failed: alGenBuffers() failed on sound '%s' (%x)", Sound->Name.Data(), Err );
    return;
  }

  alBufferData( Sound->Handle, Format, Sound->PcmData, Sound->PcmSize, Sound->SampleRate );
  if ( (Err = alGetError()) < 0 )
  {
    GLogf( LOG_WARN, "UOpenALAudioDevice::RegisterSound() failed: alBufferData() failed on sound '%s' (%x)", Sound->Name.Data(), Err );
    return;
  }
}

void UOpenALAudioDevice::RegisterMusic( UMusic* Music )
{
  // TODO
}

void UOpenALAudioDevice::UnregisterSound( USound* Sound )
{
  if ( Sound->Handle != 0 )
  {
    alDeleteBuffers( 1, &Sound->Handle );
    Sound->Handle = 0;
  }
}

void UOpenALAudioDevice::UnregisterMusic( UMusic* Music )
{
  // TODO
}

bool UOpenALAudioDevice::PlaySound( AActor* Actor, USound* Sound, FVector Location, float Volume, float Radius, float Pitch )
{
  int Err = 0;

  if ( Sound == NULL )
  {
    GLogf( LOG_WARN, "OpenALAudioDevice::PlaySound() Sound is NULL" );
    return false;
  }

  if ( Actor == NULL )
    GLogf( LOG_WARN, "OpenALAudioDevice::PlaySound() Actor is NULL" );

  if ( Sound->Handle == 0 )
    RegisterSound( Sound );

  // Create source 
  ALuint Source;
  alGenSources( 1, &Source );
  if ( (Err = alGetError()) < 0 )
  {
    GLogf( LOG_WARN, "UOpenALAudioDevice::RegisterSound() failed: alGenSources() failed on sound '%s' (%x)", Sound->Name.Data(), Err );
    return false;
  }

  alSourcei( Source, AL_BUFFER, Sound->Handle );
  if ( (Err = alGetError()) < 0 )
  {
    GLogf( LOG_WARN, "UOpenALAudioDevice::RegisterSound() failed: alSourcei() failed on sound '%s' (%x)", Sound->Name.Data(), Err );
    return false;
  }

  // Play source and add to list of sources
  alSourcef( Source, AL_GAIN, (float)SoundVolume / 255.0f );
  alSourcePlay( Source );
  Sources.PushBack( Source );

  return true;
}

bool UOpenALAudioDevice::StartMusicPlayback()
{
  for ( int i = 0; i < MusicBufferCount; i++ )
  {
    alBufferData( ALMusicBuffers[i], GetALMusicFormat(), MusicQueue->Pop(), MusicBufferSize, CurrentStreamRate );
    if ( ALIsError() )
    {
      GLogf( LOG_CRIT, "alBufferData() failed in StartMusicPlayback: %s", GetALErrorString() );
      return false;
    }

    alSourceQueueBuffers( MusicSource, 1, &ALMusicBuffers[i] );
    if ( ALIsError() )
    {
      GLogf( LOG_CRIT, "alSourceQueueBuffers() failed in StartMusicPlayback: %s", GetALErrorString() );
      return false;
    }
  }

  alSourcef( MusicSource, AL_GAIN, ((float)MusicVolume / 255.0f) * CurrentMusicVolume );

  alSourcePlay( MusicSource );
  if ( ALIsError() )
  {
    GLogf( LOG_CRIT, "alSourcePlay() failed in StartMusicPlayback: %s", GetALErrorString() );
    return false;
  }

  return true;
}

bool UOpenALAudioDevice::PlayMusicBuffer()
{
  ALint Status;
  alGetSourcei( MusicSource, AL_BUFFERS_PROCESSED, &Status );

  // Unqueue buffers as needed
  while ( Status )
  {
    ALuint Buffer;
    alSourceUnqueueBuffers( MusicSource, 1, &Buffer );

    alBufferData( Buffer, GetALMusicFormat(), MusicQueue->Pop(), MusicBufferSize, CurrentStreamRate );
    if ( ALIsError() )
    {
      GLogf( LOG_CRIT, "alBufferData() failed in PlayMusicBuffer: %s", GetALErrorString() );
      return false;
    }

    alSourceQueueBuffers( MusicSource, 1, &Buffer );
    if ( ALIsError() )
    {
      GLogf( LOG_CRIT, "alSourceQueueBuffers() failed in PlayMusicBuffer: %s", GetALErrorString() );
      return false;
    }

    Status--;
  }

  alSourcef( MusicSource, AL_GAIN, CurrentMusicVolume * ((float)MusicVolume / 255.0f) );

  alGetSourcei( MusicSource, AL_SOURCE_STATE, &Status );
  if ( Status == AL_STOPPED )
  {
    alSourcePlay( MusicSource );
    if ( ALIsError() )
    {
      GLogf( LOG_CRIT, "alSourcePlay() failed in PlayMusicBuffer: %s", GetALErrorString() );
      return false;
    }
  }

  return true;
}

bool UOpenALAudioDevice::ALIsError()
{
  Error = alGetError();
  return (Error != AL_NO_ERROR);
}

const char* UOpenALAudioDevice::GetALErrorString()
{
  switch ( Error )
  {
  case AL_NO_ERROR:
    return "AL_NO_ERROR";
  case AL_INVALID_NAME:
    return "AL_INVALID_NAME";
  case AL_INVALID_ENUM:
    return "AL_INVALID_ENUM";
  case AL_INVALID_VALUE:
    return "AL_INVALID_VALUE";
  case AL_INVALID_OPERATION:
    return "AL_INVALID_OPERATION";
  case AL_OUT_OF_MEMORY:
    return "AL_OUT_OF_MEMORY";
  default:
    return "AL_UNKNOWN_ERROR";
  }
}

ALenum UOpenALAudioDevice::GetALMusicFormat()
{
  switch ( CurrentStreamFormat )
  {
  case STREAM_Mono8:
    return AL_FORMAT_MONO8;
  case STREAM_Mono16:
    return AL_FORMAT_MONO16;
  case STREAM_Stereo8:
    return AL_FORMAT_STEREO8;
  case STREAM_Stereo16:
    return AL_FORMAT_STEREO16;
  default:
    return AL_FORMAT_UNKNOWN; // this will inevitably fail
  }
}

#include <Core/UClass.h>
IMPLEMENT_MODULE_CLASS( UOpenALAudioDevice )